{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE BangPatterns #-}

import Data.List
import Data.Maybe
import Data.Word
import Data.Time
import Data.Ratio
import Data.Either
import Data.String
import Text.Printf
import Control.Monad
import qualified Control.Parallel.Strategies as PS
import Control.Applicative
import System.IO
import System.IO.Unsafe
import System.Directory
import System.Posix.Files
import System.Environment (getArgs)
import System.FilePath.Posix
import Data.Time.Clock.POSIX as PT
import Data.Monoid hiding (Product)
import Data.Attoparsec.ByteString.Char8
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Data.ByteString.Internal as BSI
import qualified Data.Attoparsec.ByteString.Lazy as PL
import qualified Data.IntMap.Strict as M

import Network.Socket hiding (send, sendTo, recv, recvFrom)
import Network.Socket.ByteString (send, recv)

-- * Log Parsing --

data Timestamp = Timestamp { utcT :: !UTCTime,
                             usT :: !Integer } deriving (Eq, Ord, Show)
data Payload = CompactionStart { fromLevel :: !Word,
                                 fromCount :: !Word,
                                 toLevel :: !Word,
                                 toCount :: !Word }
             | CompactionEnd { fromLevel :: !Word,
                               fromCount :: !Word,
                               toLevel :: !Word,
                               toCount :: !Word,
                               bytes :: !Word }
             | Other BS.ByteString deriving (Show)
data LogEntry = LogEntry { file :: !FilePath,
                           timestamp :: !Timestamp,
                           thread :: !Integer,
                           payload :: !Payload } deriving (Show)
type Log = [LogEntry]

zeroTimestamp = Timestamp { utcT = posixSecondsToUTCTime 0, usT = 0 }

timestampDiff :: Timestamp -> Timestamp -> Integer
timestampDiff t1 t2 = utcd * 1000000 + usd
  where
    utcd = numerator $ toRational $ diffUTCTime (utcT t1) (utcT t2)
    usd = usT t1 - usT t2

parseDateTime :: Parser LocalTime
parseDateTime = do
  y <- count 4 digit
  char '/'
  mm <- count 2 digit
  char '/'
  d <- count 2 digit
  char '-'
  h <- count 2 digit
  char ':'
  m <- count 2 digit
  char ':'
  s <- count 2 digit
  return $
    LocalTime { localDay = fromGregorian (read y) (read mm) (read d),
                localTimeOfDay = TimeOfDay (read h) (read m) (read s) }

parseTimestamp :: TimeZone -> Parser Timestamp
parseTimestamp tz = do
  lt <- parseDateTime
  char '.'
  us <- decimal
  return $ Timestamp { utcT = localTimeToUTC tz lt, usT = us }

parseThreadID :: Parser Integer
parseThreadID = hexadecimal

tillEol = takeTill (isEndOfLine . BSI.c2w)

parsePayloadCStart :: Parser Payload
parsePayloadCStart = do
  "Compacting" *> skipSpace
  fc <- decimal
  char '@'
  fl <- decimal
  skipSpace <* "+" *> skipSpace
  tc <- decimal
  char '@'
  tl <- decimal
  tillEol
  return $ CompactionStart { fromLevel = fl,
                             fromCount = fc,
                             toLevel = tl,
                             toCount = tc }

parsePayloadCEnd :: Parser Payload
parsePayloadCEnd = do
  "Compacted" *> skipSpace
  fc <- decimal
  char '@'
  fl <- decimal
  skipSpace <* "+" *> skipSpace
  tc <- decimal
  char '@'
  tl <- decimal
  skipSpace <* "files =>" *> skipSpace
  b <- decimal
  skipSpace <* "bytes" *> tillEol
  return $ CompactionEnd { fromLevel = fl,
                           fromCount = fc,
                           toLevel = tl,
                           toCount = tc,
                           bytes = b}

parsePayload :: Parser Payload
parsePayload = do
  choice [parsePayloadCStart,
          parsePayloadCEnd,
          tillEol >>= return . Other]

parseEntry :: FilePath -> TimeZone -> Parser LogEntry
parseEntry f tz = do
  ts <- parseTimestamp tz
  skipSpace
  tid <- parseThreadID
  skipSpace
  payload <- parsePayload
  return $ LogEntry { file = f, timestamp = ts,
                      thread = tid, payload = payload }

parseLog :: FilePath -> TimeZone -> Parser Log
parseLog f tz = many $ parseEntry f tz <* choice [endOfLine, endOfInput]

isStartEntry LogEntry { payload = CompactionStart {} } = True
isStartEntry _ = False

isKnownEntry LogEntry { payload = (Other _) } = False
isKnownEntry _ = True

-- * Compatcions Extraction --

data CompStat = CompStat { tfirst :: !Timestamp, tlast :: !Timestamp,
                           totalCount :: !Integer,
                           totalTime :: !Integer, totalBytes :: !Integer,
                           maxTime :: !Integer, maxBytes :: !Integer } deriving Show


compactions les = concatMap (maybeToList . compaction) $ tails les
  where
    compaction (st:ends) = do
      e <- findEnd st ends
      let stt = timestamp st
      let ste = timestamp e
      let dur = timestampDiff ste stt
      let bts = toInteger $ bytes $ payload e
      return $ CompStat { tfirst = stt,
                          tlast = stt,
                          totalCount = 1,
                          totalTime = dur,
                          totalBytes = bts,
                          maxTime = dur,
                          maxBytes = bts }
    compaction _ = Nothing
    findEnd le = find (isPairEnd le)
    isPairEnd LogEntry { thread = tid1,
                         file = f1,
                         payload = CompactionStart {} }
              LogEntry { thread = tid2,
                         file = f2,
                         payload = CompactionEnd {} } | tid1 == tid2,
                                                        f1 == f2 = True
    isPairEnd _ _ = False

byHour = 3600

byTimeFrame dur = fromInteger . (`div` dur) . numerator . toRational . utcTimeToPOSIXSeconds . utcT . tfirst

compStat s1 s2 = CompStat { tfirst = tfirst s1 `min` tfirst s2,
                            tlast = tlast s1 `max` tlast s2,
                            totalCount = totalCount s1 + totalCount s2,
                            totalTime = totalTime s1 + totalTime s2,
                            totalBytes = totalBytes s1 + totalBytes s2,
                            maxTime = maxTime s1 `max` maxTime s2,
                            maxBytes = maxBytes s1 `max` maxBytes s2 }

statsForGroup :: [CompStat] -> CompStat
statsForGroup = foldl1' compStat

groupByInt keyf list =  map snd $ M.toList $ M.fromListWith (++) $ zip (map keyf list) $ map (:[]) list

analyze frame = pmap statsForGroup . group . pmapC (compactions . filter isKnownEntry)
    where group = groupByInt $ byTimeFrame frame

-- * Graphite-style render --

renderMetric :: String -> CompStat -> BS.ByteString
renderMetric host stat = BS.unlines [
                                 metric  "count" $ totalCount stat,
                                 metric  "bytes.val" $ totalBytes stat,
                                 metric  "bytes.max" $ maxBytes stat,
                                 metricf "bytes.avg" $ avg totalBytes totalCount,
                                 metric  "duration.val" $ totalTime stat,
                                 metricf "duration.avg" $ avg totalTime totalCount,
                                 metric  "duration.max" $ maxTime stat
                               ]
    where
      avg :: (CompStat -> Integer) -> (CompStat -> Integer) -> Double
      avg s1 s2 = (fromInteger . s1) stat / (fromInteger . s2) stat
      metric n v = metric1 n $ bshow v
      metricf n v = metric1 n $ BS.pack $ printf "%.4f" v
      metric1 name value = prefix <> name <> " " <> value <> " " <> ts
      bshow = fromString . show
      prefix = fromString host <> ".leveldb_logs.compaction."
      ts = bshow $ numerator $ toRational $ utcTimeToPOSIXSeconds $ utcT $ tlast stat

-- * Helper Functions --

-- | Directory enumeration
logFiles = ["LOG", "LOG.old"]

allLogFiles root = do
  ls  <- liftM (filter notDots) $ getDirectoryContents root
  lfs <- forM ls $ \name -> do
    let name' = root </> name
    fs <- getFileStatus name'
    let dir = isDirectory fs
    let log = isLogFile name && not dir
    if dir
      then allLogFiles name'
      else return $ if log then [name'] else []
  return $ concat lfs
    where
      notDots x = not $ elem x ["..", "."]
      isLogFile x = elem x logFiles

-- | Lazy monadic map to reduce memory footprint
lazyMapM f [] = return []
lazyMapM f (x:xs) = do
  y <- f x
  ys <- unsafeInterleaveIO $ lazyMapM f xs
  return (y:ys)

-- | Parallel maps
maxSparks = 50
parlf lf f = PS.withStrategy (PS.parBuffer maxSparks PS.rseq) . lf f
pmap = parlf map
pmapC = parlf concatMap

-- | Network
sendBytes :: String -> Int -> [BS.ByteString] -> IO ()
sendBytes host port bytes = withSocketsDo $ do
  addrInfo <- getAddrInfo Nothing (Just host) (Just $ show port)
  let serverAddr = head addrInfo
  sock <- socket (addrFamily serverAddr) Stream defaultProtocol
  connect sock (addrAddress serverAddr)
  forM_ bytes $ send sock
  sClose sock

defaultGraphitePort = 2003

-- * Main --

main :: IO ()
main = do
  args <- getArgs
  (logsRoot, host, renderf) <- return $ handleArgs args
  ls <- allLogFiles logsRoot
  fs <- lazyMapM readFile ls
  ps <- return $ pmap (uncurry parseRes) fs
  as <- return $ pmap (renderMetric $ normHost host) $ analyze byHour $ rights ps
  renderf as
    where
      readFile f = do { d <- BSL.readFile f; return (f, d) }
      parseRes f = reportLeft f . PL.eitherResult . PL.parse (parseLog f utc)
      reportLeft f l@(Left m) = unsafePerformIO (hPrint stderr (annErr f m)) `seq` l -- sadly the only way to reduce memory fp
      reportLeft _ r = r
      annErr f m = "Unable to parse: " ++ f ++ " " ++ m ++ "\n"
      handleArgs [lroot] = handleArgs [lroot, "localhost"]
      handleArgs [lroot, host] = (lroot, host, mapM_ BS.putStr)
      handleArgs [lroot, host, grhost] = (lroot, host, sendBytes grhost defaultGraphitePort)
      handleArgs (lroot:host:grhost:gport:_) = (lroot, host, sendBytes grhost (read gport))
      normHost = map dot2dash
                 where
                   dot2dash '.' = '-'
                   dot2dash c = c
