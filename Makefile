all: build

.PHONY: build run clean

.cabal-sandbox: ldb-logmetrics.cabal
	cabal sandbox init
	cabal install --only-dependencies

build: .cabal-sandbox ldb-logmetrics.cabal
	cabal build

run: build
	./dist/build/ldb-logmetrics/ldb-logmetrics logs localhost 

clean:
	-rm -rf .cabal-sandbox
	-rm -rf dist
	-rm -rf cabal.sandbox.config
