# LevelDB compaction log analyzer

This tool was created to make analysis of leveldb compactions possible in complex riak-like cases when there are multiple leveldb bases.
During its execution leveldb writes LOG files that contain, along with many other things, information about compactions that had happened. 
This tool aggregates that information and sends it to the graphite server.

## How to build and run

To build this project you need GHC 7.6 (or newer) and cabal with sandboxes support installed.
To build just execute `make` in the project directory 

To run the tool execute something like:
`./dist/build/ldb-logmetrics/ldb-logmetrics <logs dir> <hostname to report to graphite> [<graphite host> [<graphite port>]]`
if graphite port is not set 2003 will be used, if graphite host is not set as well the tool will produce console output 
instead of sending that data to a graphite server.
